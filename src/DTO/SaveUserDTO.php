<?php

declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

readonly class SaveUserDTO implements \JsonSerializable
{
    public function __construct(
        #[Assert\Type(type: 'int')]
        public mixed $id,

        #[Assert\NotNull(message: 'This field is required.')]
        #[Assert\NotBlank]
        #[Assert\Type(type: 'string')]
        public mixed $name,

        #[Assert\NotNull(message: 'This field is required.')]
        #[Assert\NotBlank]
        #[Assert\Type(type: 'email')]
        public mixed $email,

        #[Assert\NotNull(message: 'This field is required.')]
        #[Assert\NotBlank]
        #[Assert\Type(type: 'uuid')]
        public mixed $group_id,
    ) {
    }

    public static function fromArray(array $json): self
    {
        return new self(
            id: $json['id'] ?? null,
            name: $json['name'] ?? null,
            email: $json['email'] ?? null,
            group_id: $json['group_id'] ?? null,
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'group_id' => $this->group_id,
        ];
    }
}
