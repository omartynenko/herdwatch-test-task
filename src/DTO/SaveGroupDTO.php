<?php

declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

readonly class SaveGroupDTO implements \JsonSerializable
{
    public function __construct(
        #[Assert\Type(type: 'int')]
        public mixed $id,

        #[Assert\NotNull(message: 'This field is required.')]
        #[Assert\NotBlank]
        #[Assert\Type(type: 'string')]
        public mixed $name,
    ) {
    }

    public static function fromArray(array $json): self
    {
        return new self(
            id: $json['id'] ?? null,
            name: $json['name'] ?? null,
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
