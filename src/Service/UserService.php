<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Group;
use App\Entity\User;
use App\Repository\UserRepository;

class UserService
{
    public function __construct(
        private UserRepository $userRepository,
    ) {
    }

    public function getById(int $id): ?User
    {
        return $this->userRepository->findById($id);
    }

    public function getAll(): array
    {
        return $this->userRepository->findAll();
    }

    public function saveUser(User $user): User
    {
        $this->userRepository->saveUser($user);

        return $user;
    }

    public function removeUser(User $user): void
    {
        $this->userRepository->removeUser($user);
    }

    public function getGroupUsers(Group $group): array
    {
        $query = $this->userRepository->createQueryBuilder('hw_users')
            ->leftJoin('hw_users.group', 'hw_groups')
            ->andWhere('hw_users.group = hw_groups.id')
            ->where('hw_groups.id = :id')
            ->setParameter(':id', $group->id);

        return $query->getQuery()->getArrayResult();
    }
}
