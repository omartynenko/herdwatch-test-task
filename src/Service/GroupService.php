<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Group;
use App\Repository\GroupRepository;

class GroupService
{
    public function __construct(
        private GroupRepository $groupRepository,
    ) {
    }

    public function getById(int $id): ?Group
    {
        return $this->groupRepository->findById($id);
    }

    public function getAll(): array
    {
        return $this->groupRepository->findAll();
    }

    public function saveGroup(Group $group): Group
    {
        $this->groupRepository->saveGroup($group);

        return $group;
    }

    public function removeGroup(Group $group): void
    {
        $this->groupRepository->removeGroup($group);
    }
}
