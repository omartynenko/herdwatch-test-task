<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\GroupService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class GroupListController extends AbstractController
{
    public function __construct(
        private readonly GroupService $groupService,
    ) {
    }

    /**
     * @throws Exception
     */
    public function __invoke(): JsonResponse
    {
        $groups = $this->groupService->getAll();

        return new JsonResponse([
            'groups' => $groups, // TODO: might use a resource
        ], Response::HTTP_OK);
    }
}
