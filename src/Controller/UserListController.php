<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\UserService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UserListController extends AbstractController
{
    public function __construct(
        private readonly UserService $userService,
    ) {
    }

    /**
     * @throws Exception
     */
    public function __invoke(): JsonResponse
    {
        $users = $this->userService->getAll();

        return new JsonResponse([
            'users' => $users, // TODO: might use a resource
        ], Response::HTTP_OK);
    }
}
