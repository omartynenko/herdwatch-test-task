<?php

declare(strict_types=1);

namespace App\Controller;

use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class HealthCheckController extends AbstractController
{
    #[OA\Get(
        path: '/api/healthcheck',
        description: 'Herdwatch: healthcheck endpoint.',
        tags: ['Healthcheck'],
        responses: [
            new OA\Response(
                response: '200',
                description: 'Successful response.',
                content: new OA\JsonContent(
                    required: ['status'],
                    properties: [
                        new OA\Property(property: 'status', type: 'string', example: 'OK'),
                    ],
                    type: 'object'
                )
            ),
        ]
    )]
    public function __invoke(): JsonResponse
    {
        return new JsonResponse([
            'status' => 'OK',
        ]);
    }
}
