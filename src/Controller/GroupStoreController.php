<?php

declare(strict_types=1);

namespace App\Controller;

use App\DTO\SaveGroupDTO;
use App\Entity\Group;
use App\Service\GroupService;
use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GroupStoreController extends AbstractController
{
    public function __construct(
        private readonly GroupService $groupService,
    ) {
    }

    #[OA\Post(
        path: '/api/group',
        description: 'Herdwatch: store a group endpoint.',
        requestBody: new OA\RequestBody(
            required: true,
            content: new OA\JsonContent(
                required: ['name'],
                properties: [
                    new OA\Property(property: 'name', type: 'string', example: 'group1'),
                ]
            )
        ),
        tags: ['Group'],
        parameters: [
            new OA\Parameter(
                name: 'id',
                in: 'path',
                required: true,
                schema: new OA\Schema(type: 'string', format: 'uuid', example: '8bae705e-0a36-460c-bdb3-372601ff8cef')
            ),
        ],
        responses: [
            new OA\Response(
                response: '200',
                description: 'Successful response.',
                content: new OA\JsonContent(
                    required: ['data'],
                    type: 'object'
                )
            ),
        ]
    )]
    public function __invoke(Request $request): JsonResponse
    {
        $arr = json_decode($request->getContent(), true);

        $dto = SaveGroupDTO::fromArray($arr);

        $group = new Group(
            $dto->id,
            $dto->name,
        );

        $group = $this->groupService->saveGroup($group);

        return new JsonResponse([
            'data' => $group->jsonSerialize(), // TODO: might use a resource
        ], Response::HTTP_CREATED);
    }
}
