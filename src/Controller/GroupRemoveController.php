<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\GroupService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class GroupRemoveController extends AbstractController
{
    public function __construct(
        private readonly GroupService $groupService,
    ) {
    }

    /**
     * @throws Exception
     */
    public function __invoke(int $id): JsonResponse
    {
        $group = $this->groupService->getById($id);

        if (null === $group) {
            throw new Exception('Group not found');
        }

        $this->groupService->removeGroup($group);

        return new JsonResponse([
            'id' => $id,
        ], Response::HTTP_NO_CONTENT);
    }
}
