<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\GroupService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class GroupItemController extends AbstractController
{
    public function __construct(
        private readonly GroupService $groupService,
    ) {
    }

    /**
     * @throws Exception
     */
    public function __invoke(int $id): JsonResponse
    {
        $group = $this->groupService->getById($id);

        if (null === $group) {
            throw new Exception('Group not found');
        }

        return new JsonResponse([
            'item' => $group->jsonSerialize(), // TODO: might use a resource
        ], Response::HTTP_OK);
    }
}
