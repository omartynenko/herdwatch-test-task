<?php

declare(strict_types=1);

namespace App\Controller;

use App\DTO\SaveGroupDTO;
use App\Service\GroupService;
use Exception;
use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GroupUpdateController extends AbstractController
{
    public function __construct(
        private readonly GroupService $groupService,
    ) {
    }

    #[OA\Patch(
        path: '/api/group/{id}',
        description: 'Herdwatch: store a group endpoint.',
        requestBody: new OA\RequestBody(
            required: true,
            content: new OA\JsonContent(
                required: ['name'],
                properties: [
                    new OA\Property(property: 'name', type: 'string', example: 'group1'),
                ]
            )
        ),
        tags: ['Group'],
        parameters: [
            new OA\Parameter(
                name: 'id',
                in: 'path',
                required: true,
                schema: new OA\Schema(type: 'string', format: 'int', example: 1)
            ),
        ],
        responses: [
            new OA\Response(
                response: '200',
                description: 'Successful response.',
                content: new OA\JsonContent(
                    required: ['data'],
                    type: 'object'
                )
            ),
        ]
    )]
    /**
     * @throws Exception
     */
    public function __invoke(Request $request, int $id): JsonResponse
    {
        $arr = json_decode($request->getContent(), true);

        $dto = SaveGroupDTO::fromArray(array_merge(['id' => $id], $arr));

        $group = $this->groupService->getById($dto->id);

        if (null === $group) {
            throw new Exception('Group not found');
        }

        $group->setName($dto->name);

        $group = $this->groupService->saveGroup($group);

        return new JsonResponse([
            'data' => $group->jsonSerialize(), // TODO: might use a resource
        ], Response::HTTP_OK);
    }
}
