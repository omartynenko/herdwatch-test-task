<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\UserService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UserRemoveController extends AbstractController
{
    public function __construct(
        private readonly UserService $userService,
    ) {
    }

    /**
     * @throws Exception
     */
    public function __invoke(int $id): JsonResponse
    {
        $user = $this->userService->getById($id);

        if (null === $user) {
            throw new Exception('User not found');
        }

        $this->userService->removeUser($user);

        return new JsonResponse([
            'id' => $id,
        ], Response::HTTP_NO_CONTENT);
    }
}
