<?php

declare(strict_types=1);

namespace App\Controller;

use App\DTO\SaveUserDTO;
use App\Service\GroupService;
use App\Service\UserService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserUpdateController extends AbstractController
{
    public function __construct(
        private readonly UserService $userService,
        private readonly GroupService $groupService,
    ) {
    }

    /**
     * @throws Exception
     */
    public function __invoke(Request $request, int $id): JsonResponse
    {
        $arr = json_decode($request->getContent(), true);

        $dto = SaveUserDTO::fromArray(array_merge(['id' => $id], $arr));

        $user = $this->userService->getById($dto->id);
        if (null === $user) {
            throw new Exception('User not found');
        }

        $group = $this->groupService->getById($dto->group_id);
        if (null === $group) {
            throw new Exception('Group not found');
        }

        $user->setName($dto->name)
            ->setEmail($dto->email)
            ->setGroup($group);

        $user = $this->userService->saveUser($user);

        return new JsonResponse([
            'data' => $user->jsonSerialize(), // TODO: might use a resource
        ], Response::HTTP_OK);
    }
}
