<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\UserService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UserItemController extends AbstractController
{
    public function __construct(
        private readonly UserService $userService,
    ) {
    }

    /**
     * @throws Exception
     */
    public function __invoke(int $id): JsonResponse
    {
        $user = $this->userService->getById($id);

        if (null === $user) {
            throw new Exception('User not found');
        }

        return new JsonResponse([
            'item' => $user->jsonSerialize(), // TODO: might use a resource
        ], Response::HTTP_OK);
    }
}
