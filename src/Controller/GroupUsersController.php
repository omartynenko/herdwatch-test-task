<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\GroupService;
use App\Service\UserService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class GroupUsersController extends AbstractController
{
    public function __construct(
        private readonly UserService $userService,
        private readonly GroupService $groupService,
    ) {
    }

    /**
     * @throws Exception
     */
    public function __invoke(int $id): JsonResponse
    {
        $group = $this->groupService->getById($id);

        if (null === $group) {
            throw new Exception('Group not found');
        }

        $users = $this->userService->getGroupUsers($group);

        return new JsonResponse([
            'users' => $users,
        ], Response::HTTP_OK);
    }
}
