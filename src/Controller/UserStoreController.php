<?php

declare(strict_types=1);

namespace App\Controller;

use App\DTO\SaveUserDTO;
use App\Entity\User;
use App\Service\GroupService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserStoreController extends AbstractController
{
    public function __construct(
        private readonly UserService $userService,
        private readonly GroupService $groupService,
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $arr = json_decode($request->getContent(), true);

        $dto = SaveUserDTO::fromArray($arr);

        $group = $this->groupService->getById($dto->group_id); // TODO: check&throw an exception

        $user = new User(
            $dto->id,
            $dto->name,
            $dto->email,
            $group
        );

        $user = $this->userService->saveUser($user);

        return new JsonResponse([
            'data' => $user->jsonSerialize(), // TODO: might use a resource
        ], Response::HTTP_CREATED);
    }
}
