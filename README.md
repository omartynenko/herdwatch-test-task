# herdwatch-test-task

## 1. Create `.env` file:
```
cp .env.dist .env
```

## 2. Update `.env` file:
```
APP_ENV=dev
```

## 3. Build the docker images:
```
docker-compose build
```

## 4. Build the docker containers:
```
docker-compose up -d
```

## 5. Healthcheck request:
```
GET http://localhost:82/api/healthcheck
```
Response:
```
{
    "status": "OK"
}
```
```
curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:82/api/healthcheck
```

## 6. Group store request:
```
POST http://localhost:82/api/group
{
    "name": "groupA"
}
```
Response:
```
{
    "data": {
        "id": 13,
        "name": "groupA",
        "createdAt": {
            "date": "2024-05-08 04:41:35.974628",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "updatedAt": {
            "date": "2024-05-08 04:41:35.974629",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "users": {}
    }
}
```
```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"name": "groupA"}' \
  http://localhost:82/api/group
```

## 7. Group update request:
```
PATCH http://localhost:82/api/group/13
{
    "name": "groupB"
}
```
Response:
```
{
    "data": {
        "id": 13,
        "name": "groupB",
        "createdAt": {
            "date": "2024-05-08 04:41:35.000000",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "updatedAt": {
            "date": "2024-05-08 04:44:34.647627",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "users": {}
    }
}
```
```
curl --header "Content-Type: application/json" \
  --request PATCH \
  --data '{"name": "groupB"}' \
  http://localhost:82/api/group/13
```

## 8. Group remove request:
```
DELETE http://localhost:82/api/group/13
```
Response: (204 http code)
```
curl --header "Content-Type: application/json" \
  --request DELETE \
  http://localhost:82/api/group/3
```


## 9. Group list request:
```
GET http://localhost:82/api/group
```
Response:
```
{
    "groups": [
        {
            "id": 1,
            "name": "group1",
            "createdAt": {
                "date": "2024-05-07 20:20:59.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "updatedAt": {
                "date": "2024-05-07 20:20:59.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "users": {}
        },
        ...
        ...
        ...
    ]
}
```
```
curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:82/api/group
```

## 10. User store request:
```
POST http://localhost:82/api/user
{
    "name": "user1",
    "email": "user1@email.com",
    "group_id": 1
}
```
Response:
```
{
    "data": {
        "id": 7,
        "name": "user1",
        "email": "user1@email.com",
        "createdAt": {
            "date": "2024-05-08 04:51:02.648040",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "updatedAt": {
            "date": "2024-05-08 04:51:02.648041",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "group": {
            "id": 1,
            "name": "group1",
            "createdAt": {
                "date": "2024-05-07 20:20:59.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "updatedAt": {
                "date": "2024-05-07 20:20:59.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "users": {}
        }
    }
}
```
```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"name": "user1", "email": "user1@email.com", "group_id": 1}' \
  http://localhost:82/api/user
```

## 11. User update request:
```
PATCH http://localhost:82/api/user/7
{
    "name": "user7",
    "email": "user7@email.com",
    "group_id": 1
}
```
Response:
```
{
    "data": {
        "id": 7,
        "name": "user7",
        "email": "user1@emai7.com",
        "createdAt": {
            "date": "2024-05-08 04:51:02.000000",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "updatedAt": {
            "date": "2024-05-08 04:57:00.652940",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "group": {
            "id": 1,
            "name": "group1",
            "createdAt": {
                "date": "2024-05-07 20:20:59.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "updatedAt": {
                "date": "2024-05-07 20:20:59.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "users": {}
        }
    }
}
```
```
curl --header "Content-Type: application/json" \
  --request PATCH \
  --data '{"name": "user77", "email": "user77@email.com", "group_id": 1}' \
  http://localhost:82/api/user/7
```

## 12. Group users request:
```
GET http://localhost:82/api/group/1/users
```
Response:
```
{
    "users": [
        {
            "id": 7,
            "name": "user7",
            "email": "user1@emai7.com",
            "createdAt": {
                "date": "2024-05-08 04:51:02.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "updatedAt": {
                "date": "2024-05-08 04:57:00.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            }
        }
    ]
}
```
```
curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:82/api/group/1/users
```

## 13. User list request:
```
GET http://localhost:82/api/user
```
Response:
```
{
    "users": [
        {
            "id": 1,
            "name": "user1",
            "email": "user1@email.com",
            "createdAt": {
                "date": "2024-05-07 20:48:51.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "updatedAt": {
                "date": "2024-05-07 20:48:51.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "group": {
                "id": 8,
                "name": "group1",
                "createdAt": {
                    "date": "2024-05-07 20:42:20.000000",
                    "timezone_type": 3,
                    "timezone": "UTC"
                },
                "updatedAt": {
                    "date": "2024-05-07 20:42:20.000000",
                    "timezone_type": 3,
                    "timezone": "UTC"
                },
                "users": {}
            }
        },
        ...
        ...
        ...
    ]
}
```
```
curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:82/api/user
```

## 14. User item request:
```
GET http://localhost:82/api/user/1
```
Response:
```
{
    "item": {
        "id": 1,
        "name": "user1",
        "email": "user1@email.com",
        "createdAt": {
            "date": "2024-05-07 20:48:51.000000",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "updatedAt": {
            "date": "2024-05-07 20:48:51.000000",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "group": {
            "id": 8,
            "name": "group1",
            "createdAt": {
                "date": "2024-05-07 20:42:20.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "updatedAt": {
                "date": "2024-05-07 20:42:20.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "users": {}
        }
    }
}
```
```
curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:82/api/user/1
```

## 14. User remove request:
```
DELETE http://localhost:82/api/user/1
```
Response: (204 http code)
```
curl --header "Content-Type: application/json" \
  --request DELETE \
  http://localhost:82/api/user/1
```